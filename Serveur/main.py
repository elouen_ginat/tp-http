from data import mots
from metier import Mot

from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    return {"hello"}

@app.get("/mots")
def get_all_words():
         return mots

@app.post("/mot")
async def add_word(mot:Mot):
    mots.append(mot)
    return mot
